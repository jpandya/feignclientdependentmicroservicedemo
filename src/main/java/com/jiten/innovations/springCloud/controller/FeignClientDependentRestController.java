package com.jiten.innovations.springCloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignClientDependentRestController {

	@Value("${msg:Hello world - Config Server is not working..please check}")
	private String msg;

	@RequestMapping("/msg")
	public String getMsg() {
		return msg;
	}
	
	
	
}
